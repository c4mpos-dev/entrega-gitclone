// Nomenclatura de variáveis

const list = [
    {
      title: 'User',
      followers: 5
    },
    {
      title: 'Friendly',
      followers: 50,
    },
    {
      title: 'Famous',
      followers: 500,
    },
    {
      title: 'Super Star',
      followers: 1000,
    },
  ]

  export default async function getDataOfGithubUser(req, res) {
    const githubUser = String(req.query.username)

    if (!githubUser) {
      return res.status(400).json({
        message: `Please provide an username to search on the github API`
      })
    }

    const responseGithub = await fetch(`https://api.github.com/users/${githubUser}`);

    if (responseGithub.status === 404) {
      return res.status(400).json({
        message: `User with username "${githubUser}" not found`
      })
    }

    const dataToJson = await responseGithub.json()

    const orderList = list.sort((a, b) =>  b.followers - a.followers);

    const categoryOfList = orderList.find(i => dataToJson.followers > i.followers)

    const fameLevelResult = {
      githubUser,
      categoryOfList: categoryOfList.title
    }

    return fameLevelResult
  }

  getDataOfGithubUser({ query: {
    username: 'josepholiveira'
  }}, {})